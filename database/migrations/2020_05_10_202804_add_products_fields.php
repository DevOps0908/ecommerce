<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
        $table->integer('category_id')->after('id');
        $table->string('product_name')->after('category_id');
        $table->string('product_code')->after('product_name');
        $table->string('product_quantity')->after('product_code');
        $table->text('description')->after('product_quantity');
        $table->float('price')->after('description');
        $table->string('image')->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
