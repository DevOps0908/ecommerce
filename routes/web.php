<?php

/* Start Frontend Routes */
Route::get('/','IndexController@index');

// Category / Listing Page
Route::get('/products/{url}', 'ProductsController@products');

// Product Details Page
Route::get('/product/{id}', 'ProductsController@product');

/* Ends of Frontend Routes */



/* Backend Routes */
Route::match(['get','post'], 'admin/', 'AdminController@login');

Auth::routes();

//Home Page
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' =>['auth']], function(){

	Route::get('/admin/dashboard', 'AdminController@dashboard');
	Route::get('/admin/settings', 'AdminController@settings');
	Route::get('/admin/check-pwd','AdminController@chkPassword');
	Route::match(['get','post'],'/admin/update-pwd','AdminController@updatePassword');

	// Categories Routes (Admin)
	Route::match(['get', 'post'], '/admin/add-category', 'CategoryController@addCategory');
	Route::match(['get', 'post'], '/admin/edit-category/{id}', 'CategoryController@editCategory');
	Route::match(['get', 'post'], '/admin/delete-category/{id}', 'CategoryController@deleteCategory');
	Route::get('/admin/view-categories', 'CategoryController@viewCategories');

	// Products Routes
	Route::match(['get','post'], '/admin/add-product','ProductsController@addProduct');
	Route::match(['get','post'], '/admin/edit-product/{id}','ProductsController@editProduct');
	Route::get('/admin/view-products','ProductsController@viewProducts');
	Route::get('/admin/delete-product/{id}', 'ProductsController@deleteProduct');
	Route::get('/admin/delete-product-image/{id}', 'ProductsController@deleteProductImage');

	//Products Attributes Routes
	Route::match(['get','post'], '/admin/add-attributes/{id}','ProductsController@addAttributes');
	Route::get('/admin/delete-attributes/{id}','ProductsController@deleteAttributes');

});

Route::get('/logout', 'AdminController@logout');



