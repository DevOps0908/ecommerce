<?php

namespace App\Services;

Class UserService
{
	public function hendleUserLogin()
	{
		$this->updatedUserLastSeen();
	
		$this->updateUserActivityTime();
	}

	Private function updatedUserLastSeen()
	{
		$now = now();
		logger("User last seen {$now}");
	}

	Private function updateUserActivityTime()
	{
		$now = now();
		logger("User last activity was at {$now}");
	}

}