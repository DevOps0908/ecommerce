<?php

namespace App\Services\MessageService;


class SendMessageSMS implements SendMessageInterface
{
	 
	public function sendMessage($user, $message)
	{
		logger("User say {$message} to {$user} through SMS.");
	}
}