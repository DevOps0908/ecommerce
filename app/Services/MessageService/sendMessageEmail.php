<?php

namespace App\Services\MessageService;


class SendMessageEmail implements SendMessageInterface
{
	 
	public function sendMessage($user, $message)
	{
		logger("User say {$message} to {$user} through Email.");
	}
}