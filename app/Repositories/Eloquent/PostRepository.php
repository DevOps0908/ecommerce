<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\PostRepositoryInterface;
use App\Models\Post;
use DB;
 
class PostRepository implements PostRepositoryInterface
{
   
	private $post;
 
    /** PostRepositoryInterface constructor. **/

    public function __construct(Post $post)
    {
        $this->post = $post;
    }


    /** Get all posts. **/

    public function all($columns = array('*')) {
        return $this->post->get($columns);
    }

	public function paginate($perPage = null, $columns = array('*'))
	{
		//return Post::paginate($perPage, $columns);
		return $this->post->paginate($perPage, $columns);
	} 

 	
 	/** Get post by id. **/

    public function find($id, $columns = array('*')) 
    {
        return $this->post->find($id, $columns);
    }


    public function findBy($field, $value, $columns = array('*'))
    {
        //return $this->post->find($id);
    }
 
	public function findOrFail($id, $columns = array('*'))
	{
		return $this->post->findOrFail($id, $columns);
	}


	/** Create a new post. **/
 
    public function create(array $attributes)
    {
        return $this->post->create($attributes);
    }

	/** Update a post. **/ 

    public function update($id, array $attributes)
    {
        return $this->post->find($id)->update($attributes);
    }

	/** Delete a post. **/ 

    public function delete($id)
    {
        return $this->post->find($id)->delete();
    }	
}