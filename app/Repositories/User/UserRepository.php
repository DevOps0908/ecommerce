<?php

namespace App\Repositories\User;

use App\Repositories\User\UserInterface;
use App\Models\User;
use DB;

class UserRepository implements UserInterface
{
    public $user;


    /** UserRepository constructor. **/

    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /** Get all users. **/

    public function paginate($perPage = null, $columns = array('*'))
    {
        return $this->user->paginate($perPage, $columns);
    } 


    /** Get user by id. **/
 
    public function findOrFail($id, $columns = array('*'))
    {
        return $this->user->findOrFail($id, $columns);
    }

    
    /** Create a new post. **/
 
    public function create(array $attributes)
    {
        return $this->user->create($attributes);
    }    
 

    /** Update a post. **/ 

    public function update($id, array $attributes)
    {
        return $this->user->findOrFail($id)->update($attributes);
    } 


    /** Delete a post. **/ 

    public function delete($id)
    {
        return $this->user->findOrFail($id)->delete();
    }   
}