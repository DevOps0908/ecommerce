<?php

namespace App\Repositories;

Class AbstractRepository implements AbstractInterface
{

	private $model;
 
    /** PostRepositoryInterface constructor. **/

    public function __construct(Model $model)
    {
        $this->model = $model;
    }


    /** Get all posts. **/

    public function all() 
    {
        
        return $this->model->select()
        	->orderBy('created_at', 'desc')
        	->get();

    }

	public function paginate($limit)
	{
        return $this->model->select()
        	->orderBy('created_at', 'desc')
        	->paginate($limit);
	} 

 	
 	/** Get post by id. **/

    public function find($id) 
    {
        return $this->model->where('id', $id)->first();
    }


	/** Create a new post. **/
 
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

	/** Update a post. **/ 

    public function update(array $attributes, $id, $getDataBack = false)
    {
    	$update = $this->model->where('id', $id)->update($attributes);
    	if($getDataBack){
    		$update = $this->find($id);
    	}
        return $update;
    }

	/** Delete a post. **/ 

    public function remove($id)
    {
        return $this->model->where('id', $id)->delete();
    }	

}