<?php
namespace App\Repositories\Contracts;


interface PostRepositoryInterface
{
    public function all($columns = array('*'));

    public function paginate($perPage = 5, $columns = array('*'));
 
    public function create(array $attributes);
 
    public function update($id, array $attributes);
 
    public function delete($id);

    public function find($id, $columns = array('*'));

    public function findOrFail($id, $columns = array('*'));

    public function findBy($field, $value, $columns = array('*'));
}