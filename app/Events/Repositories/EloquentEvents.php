<?php

namespace App\Events\Repositories;

use Carbon\Carbon;
use illuminate\Http\Request;
use illuminate\Support\Facades\Validator;
use illuminate\Support\Str;
use App\Repositories\AbstractRepository

Class EloquentEvents extends AbstractRepository implements EventsRepository
{


	protected $model;
 

    public function __construct(Model $model)
    {
        $this->model = $model;
    }


	public function getUpcomingEvents()
	{

		return $this->model
			->where('end_date', '>', Carbon::today()->formate(formate:'Y-m-d'))
    		->orderBy('start_date', 'asc')
    		->get();

	}

	public function getPastEvents()
	{

		return $this->model
	        ->where('end_date', '<', Carbon::today()->formate(formate:'Y-m-d'))
			->orderBy('start_date', 'asc')
			->limit(3)
			->get();

	}

}