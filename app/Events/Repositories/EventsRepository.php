<?php

namespace App\Events\Repositories;

use App\Repositories\AbstractInterface

interface EventsRepository extends AbstractInterface
{

	public function getUpcomingEvents();
	public function getPastEvents();

}