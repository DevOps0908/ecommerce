<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;

class IndexController extends Controller
{
    public function index()
    {
    	// In Ascending order (by default)
    	$productsAll = Product::get();

    	// In Descending order
    	$productsAll = Product::orderBy('id','DESC')->get();

    	//In Random order
    	$productsAll = Product::inRandomOrder()->get();

    	//Get all Categories and Sub Categories
    	$categories = Category::with('categories')->where(['parent_id'=>0])->get();
/*    	$categories = json_decode(json_encode($categories));
    	echo "<pre>"; print_r($categories); die;*/
/*    	$categories_menu = "";
    	foreach ($categories as $cat){
			$categories_menu .= "<li class='{{ $cat->parent_id = 0 ? 'dropdown' : ''  }}'>
									<a href='#' class='{{ $cat->children->count() = 0 ? 'dropdown-toggle' : ''  }}' data-toggle='dropdown'>".$cat->name." <span class='caret'></span>
									</a>

			                	<div class='dropdown-menu w3ls_vegetables_menu'>
									<div class='w3ls_vegetables'>
										<ul>";
										$sub_categpries = Category::where(['parent_id'=>$cat->id])->get();
										foreach ($sub_categpries as $subcat) {
											$categories_menu .= "<li><a href='".$subcat->id."'>".$subcat->name."</a></li>";
										}

						
					$categories_menu .= "</ul>
						                 </div>
						            </div>
						        </li>";    	
    	}*/
    	return view('index')->with(compact('productsAll','categories_menu','categories'));
    }
}
