<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use App\Repositories\User\UserInterface;
use App\Repositories\User\PostRepository;
use DB;
use Hash;

class UserController extends Controller
{
    
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    
    /** Display a listing of the resource. **/

    public function index($id = null)
    {       
        $users = $this->user->paginate(5);
        return view('users.index', compact('users'))
                ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    public function guard()
    {
        $user = Auth::guard('admin')->user();
        // Or...
        $user = auth()->guard('admin')->user();
        
        dd($user);
    }


    /** Create a post **/

    public function create(Request $request)
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));        
    } 

    /** Store a newly created resource in storage. **/

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);


        $attributes = $request->all();
        $attributes['password'] = Hash::make($attributes['password']);


        $user = $this->user->create($attributes);
        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')
                        ->with('success','User created successfully');
    }


    /** Display the specified resource. **/

    public function show($id)
    {
        $user = $this->user->findOrFail($id);
        return view('users.show',compact('user'));
    }

    
    /** Show the form for editing the specified resource. **/

    public function edit($id)
    {
        $user = $this->user->findOrFail($id);
        
        $roles = Role::pluck('name','name')->all();
        
        $userRole = $user->roles->pluck('name','name')->all();

        return view('users.edit',compact('user','roles','userRole'));
    }


    /** Update the specified resource in storage. **/

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);


        $attributes = $request->all();

        if(!empty($attributes['password'])){ 

            $attributes['password'] = Hash::make($attributes['password']);
        }else{

            $attributes = array_except($attributes,array('password'));    
        }


        $user = $this->user->findOrFail($id);
        $user->update($attributes);

        DB::table('model_has_roles')->where('model_id',$id)->delete();


        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }  


    /** Remove the specified resource from storage. **/

    public function destroy($id)
    {
        $this->user->findOrFail($id)->delete();
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }  
}
