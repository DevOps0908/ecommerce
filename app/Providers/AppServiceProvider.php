<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\MessageService\SendMessageInterface', 
            'App\Services\MessageService\SendMessageEmail'
        );

        $this->app->bind(
            'App\Events\Repositories\EventsRepository', 
            'App\Events\Repositories\EloquentEvents'
        );
    }
}
