@extends('layouts.frontLayout.front_design')

@section('content')

<?php
use App\Http\Controllers\Controller;
$mainCategories = Controller::mainCategories();
?>
<!-- banner -->
	<div class="banner">
		<div class="w3l_banner_nav_left">
			<nav class="navbar nav_bottom">
			 <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="navbar-header nav_2">
				  <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
			   </div> 
			   <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
					<ul class="nav navbar-nav nav_1">
						<?php //echo $categories_menu; ?>
						@foreach($categories as $cat)
						@if($cat->status=="1")
						<li class="dropdown mega-dropdown active">
							<a href="#{{ $cat->id }}" class="dropdown-toggle" data-toggle="dropdown">{{ $cat->name }}<span class="caret"></span></a>				
							<div class="dropdown-menu mega-dropdown-menu w3ls_vegetables_menu">
								<div class="w3ls_vegetables">
									<ul>	
										@foreach($cat->categories as $subcat)
											@if($subcat->status=="1")
												<li><a href="{{ asset('products/'.$subcat->url) }}">{{ $subcat->name }}</a></li>
											@endif
										@endforeach
									</ul>
								</div>                  
							</div>				
						</li>
						@endif
						@endforeach
					</ul>
				 </div><!-- /.navbar-collapse -->
			</nav>
		</div>
		<div class="w3l_banner_nav_right">
			<section class="slider">
				<div class="flexslider">
					<ul class="slides">
						<li>
							<div class="w3l_banner_nav_right_banner">
								<h3>Make your <span>food</span> with Spicy.</h3>
								<div class="more">
									<a href="{{url('products')}}" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</li>
						<li>
							<div class="w3l_banner_nav_right_banner1">
								<h3>Make your <span>food</span> with Spicy.</h3>
								<div class="more">
									<a href="{{url('products')}}" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</li>
						<li>
							<div class="w3l_banner_nav_right_banner2">
								<h3>upto <i>50%</i> off.</h3>
								<div class="more">
									<a href="{{url('products')}}" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</section>
			<!-- flexSlider -->
				<link rel="stylesheet" href="{{ asset('css/frontend_css/flexslider.css') }}" type="text/css" media="screen" property="" />
				<script defer src="{{ asset('js/frontend_js/jquery.flexslider.js')}}"></script>
				<script type="text/javascript">
				$(window).load(function(){
				  $('.flexslider').flexslider({
					animation: "slide",
					start: function(slider){
					  $('body').removeClass('loading');
					}
				  });
				});
			  </script>
			<!-- //flexSlider -->
		</div>
		<div class="clearfix"></div>
	</div>
<!-- banner -->

<!-- top-brands -->
	<div class="top-brands">
		<div class="container">
			<h3>Hot Offers</h3>
			<div class="agile_top_brands_grids">
				@foreach($productsAll as $product)
				<div class="col-md-3 top_brand_left">
					<div class="hover14 column">
						<div class="agile_top_brand_left_grid">
							<div class="tag"><img src="images/frontend_images/tag.png" alt=" " class="img-responsive" /></div>
							<div class="agile_top_brand_left_grid1">
								<figure>
									<div class="snipcart-item block" >
										<div class="snipcart-thumb">
											<a href="{{ url('product/'.$product->id) }}"><img title=" " alt=" " src="{{ asset('images/backend_images/products/small/'.$product->image) }}" width="100px;" height="120px;" /></a>		
											<p>{{ $product->product_name }}</p>
											<h4>₹ {{ $product->price }} <span>₹10.00</span></h4>
										</div>
										<div class="snipcart-details top_brand_home_details">
											<form action="checkout.html" method="post">
												<fieldset>
													<input type="hidden" name="cmd" value="_cart" />
													<input type="hidden" name="add" value="1" />
													<input type="hidden" name="business" value=" " />
													<input type="hidden" name="item_name" value="Fortune Sunflower Oil" />
													<input type="hidden" name="amount" value="7.99" />
													<input type="hidden" name="discount_amount" value="1.00" />
													<input type="hidden" name="currency_code" value="USD" />
													<input type="hidden" name="return" value=" " />
													<input type="hidden" name="cancel_return" value=" " />
													<input type="submit" name="submit" value="Add to cart" class="button" />
												</fieldset>	
											</form>
										</div>
									</div>
								</figure>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				<div class="clearfix"> </div>
			</div>
			
		</div>
	</div>
<!-- //top-brands -->


<!-- fresh-vegetables -->
<div class="fresh-vegetables">
	<div class="container">
		<h3>Top Products</h3>
		<div class="w3l_fresh_vegetables_grids">
			<div class="col-md-3 w3l_fresh_vegetables_grid w3l_fresh_vegetables_grid_left">
				<div class="w3l_fresh_vegetables_grid2">
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i><a href="#">All Brands</a></li>
						@foreach($mainCategories as $cat)
							@if($cat->status=="1")
								<li><i class="fa fa-check" aria-hidden="true"></i><a href="{{ asset('products/'.$cat->url) }}">{{ $cat->name }}</a></li>
							@endif
						@endforeach
					</ul>
				</div>
			</div>
			<div class="col-md-9 w3l_fresh_vegetables_grid_right">
				<div class="col-md-4 w3l_fresh_vegetables_grid">
					<div class="w3l_fresh_vegetables_grid1">
						<img src="{{ asset('images/frontend_images/8.jpg')}}" alt=" " class="img-responsive" />
					</div>
				</div>
				<div class="col-md-4 w3l_fresh_vegetables_grid">
					<div class="w3l_fresh_vegetables_grid1">
						<div class="w3l_fresh_vegetables_grid1_rel">
							<img src="{{ asset('images/frontend_images/7.jpg')}}" alt=" " class="img-responsive" />
							<div class="w3l_fresh_vegetables_grid1_rel_pos">
								<div class="more m1">
									<a href="{{url('products')}}" class="button--saqui button--round-l button--text-thick" data-text="Shop now">Shop now</a>
								</div>
							</div>
						</div>
					</div>
					<div class="w3l_fresh_vegetables_grid1_bottom">
						<img src="{{ asset('images/frontend_images/10.jpg')}}" alt=" " class="img-responsive" />
						<div class="w3l_fresh_vegetables_grid1_bottom_pos">
							<h5>Special Offers</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 w3l_fresh_vegetables_grid">
					<div class="w3l_fresh_vegetables_grid1">
						<img src="{{ asset('images/frontend_images/9.jpg')}}" alt=" " class="img-responsive" />
					</div>
					<div class="w3l_fresh_vegetables_grid1_bottom">
						<img src="{{ url('/images/frontend_images/11.jpg') }}" alt="" class="img-responsive" />
					</div>
				</div>
				<div class="clearfix"> </div>
				<div class="agileinfo_move_text">
					<div class="agileinfo_marquee">
						<h4>get <span class="blink_me">25% off</span> on first order and also get gift voucher</h4>
					</div>
					<div class="agileinfo_breaking_news">
						<span> </span>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- //fresh-vegetables -->


<!-- newsletter -->
	<div class="newsletter">
		<div class="container">
			<div class="w3agile_newsletter_left">
				<h3>sign up for our newsletter</h3>
			</div>
			<div class="w3agile_newsletter_right">
				<form action="#" method="post">
					<input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
					<input type="submit" value="subscribe now">
				</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //newsletter -->

@endsection